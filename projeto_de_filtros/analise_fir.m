% vamos usar um filtro do matlab chamado de fir1 ele tem que ser passar , se não colocar nada ele já diz que vai ser hamming 
%%ha hora de colocar a hjanela colocar o tamanho

clear all;close all;clc
No =126;
fs= 800;
w = hann(No+1);
h=fir1(No, [126/800]*2,w);%eles trabalham com frequência angular normalizada pelo pi ou seja normalizada dividi
%da pelo pi
n = 0:No;
stem(n,h);
h = fft(h,100000);
plot(linspace(-0.5, 0.5,length(h))*fs,fftshift(abs(h)))
hold on;
plot(126,0.5,'x','markersize',10)
plot(126,-0.5,'x','markersize',10)
grid on

clear all; close all; clc

for f0 = 3500;
    % Definição de sinais senoidais
    % f0 = 300; % em hertz
    duracao_segundos = 10 / f0; % 100 períodos
    amplitude = 5;
    
    % No tempo contínuo, a transformada de Fourier desse sinal tem dois impulsos: um em f0=300Hz, outro em -f0=-300Hz.
    
    % Vamos amostrar o sinal e calcular a DFT:
    fs = 5000; % maior que a frequência de Nyquist, que é 600 hertz para esse sinal.
    t = 0 : 1 / fs : duracao_segundos;
    x = amplitude * sin(2 * pi * f0 * t);
    
    % Sinal no tempo
    % plot(t, x);
    % xlabel('Tempo t (segundos)')
    % ylabel('x_c(t)')
    % grid on;
    % pause
    
    % Análise de Fourier
    % DFT:
    Xdft = fft(x);
    N = length(Xdft);
    % plot(0 : N - 1, abs(Xdft));
    % xlabel('Índice de frequência k')
    % ylabel('|X_{DFT}[k]|')
    % grid on;
    % Observe que o janelamento que fizemos (janelamento retangular) causa um espalhamento espectral.
    
    % FT:
    f_normalizadas = linspace(-0.5, 0.5, N);
    f = f_normalizadas * fs;
    plot(f, fftshift(abs(Xdft)) / fs);
    xlabel('Frequência f (hertz)')
    ylabel('|X(f)|')
    grid on;
    % Observe que o janelamento que fizemos (janelamento retangular) causa um espalhamento espectral.
    pause(0.05);
    % Note o efeito de aliasing!!!
end

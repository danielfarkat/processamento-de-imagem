clear all; close all; clc

x = 1 : 10;
N = length(x);
n = 0 : N - 1;

plot(n, x)
xlabel('Índices de tempo n')
ylabel('x[n]')
grid on

% Cálculo da transformada de Fourier em tempo discreto (DTFT)
% x_hat(f): somatório com n de -infinito a infinito de x[n]*exp(-j*2*pi*f*n), para qualquer f real

f = linspace(-3, 3, 100000); % f na teoria é variável contínua; estamos calculando em um número elevado de pontos.
x_hat = zeros(size(f));
for k = 1 : length(f)
    x_hat(k) = sum(x .* exp(-j * 2 * pi * f(k) * n));
end

plot(f, abs(x_hat))
xlabel('Frequências lineares normalizadas f')
ylabel('|X_{DTFT}(f)|')
grid on
hold on;
discrete_frequencies = 0 : 1 / N : (1 - 1 / N);
plot(discrete_frequencies, abs(fft(x)), 'Xr', 'markersize', 10)
legend('DTFT de x', 'DFT de x')


% figure;
% plot(f, unwrap(angle(x_hat)) + 27 * 2 * pi)
% xlabel('Frequências lineares normalizadas f')
% ylabel('\angle X_{DTFT}(f)')
% grid on

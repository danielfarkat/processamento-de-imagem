clear all; close all; clc

[x, fs] = audioread('a1.wav');
N = length(x);
n = 0 : N - 1;
t = n * 1.0 / fs;
% plot(t, x);
% pause;

[x, fs] = audioread('a4.wav');
x = x(:, 1);
N = length(x);
n = 0 : N - 1;
t = n * 1.0 / fs;
% plot(t, x);
% pause;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Análise de Fourier dos dois sinais
% Primeiro instrumento musical (diapasão)
%{
[x, fs] = audioread('a1.wav');
N = length(x);
n = 0 : N - 1;
t = n * 1.0 / fs;
X = fft(x); % Cálculo da DFT na forma não-normalizada
plot(0 : N - 1, abs(X))
grid on;
xlabel('Índice de frequência k')
ylabel('|X_{DFT}[k]|')
title('Transformada discreta de Fourier (DFT), em módulo, de um sinal de aúdio do diapasão')
figure;
% f_normalizada = linspace(0, 1, N + 1);
% f_normalizada = f_normalizada(1 : N); % N pontos entre 0 (inclusive) e 1 (exclusive)
% plot(f_normalizada, abs(X));
f_normalizada = linspace(-0.5, 0.5, N);
plot(f_normalizada, fftshift(abs(X)))
a = axis();
axis([-0.06 0.06 a(3) a(4)])
xlabel('Frequência linear normalizada f')
ylabel('|X_{DTFT}(f)|')
title('Transformada de Fourier em tempo discreto (DTFT), em módulo, de um sinal de aúdio do diapasão')
grid on;
figure;
f = f_normalizada * fs; % desnormalização das frequências
Xft = X / fs; % Compensação da mudança de escala da transformada contínua de Fourier para a DTFT
plot(f, fftshift(abs(Xft)))
xlabel('Frequência linear f (hertz)')
ylabel('|X_{FT}(f)|')
title('Transformada contínua de Fourier (FT), em módulo, de um sinal de aúdio do diapasão')
grid on;
a = axis();
axis([-0.06 * fs 0.06 * fs a(3) a(4)])
xticks = get(gca, 'Xtick')
set(gca, 'Xtick', sort([xticks -440 440]));
%}

% Quarto instrumento musical (oboé)
[x, fs] = audioread('a4.wav');
N = length(x);
n = 0 : N - 1;
t = n * 1.0 / fs;
X = fft(x); % Cálculo da DFT na forma não-normalizada
plot(0 : N - 1, abs(X))
grid on;
xlabel('Índice de frequência k')
ylabel('|X_{DFT}[k]|')
title('Transformada discreta de Fourier (DFT), em módulo, de um sinal de aúdio do oboé')
figure;
% f_normalizada = linspace(0, 1, N + 1);
% f_normalizada = f_normalizada(1 : N); % N pontos entre 0 (inclusive) e 1 (exclusive)
% plot(f_normalizada, abs(X));
f_normalizada = linspace(-0.5, 0.5, N);
plot(f_normalizada, fftshift(abs(X)))
a = axis();
axis([-0.06 0.06 a(3) a(4)])
xlabel('Frequência linear normalizada f')
ylabel('|X_{DTFT}(f)|')
title('Transformada de Fourier em tempo discreto (DTFT), em módulo, de um sinal de aúdio do oboé')
grid on;
figure;
f = f_normalizada * fs; % desnormalização das frequências
Xft = X / fs; % Compensação da mudança de escala da transformada contínua de Fourier para a DTFT
plot(f, fftshift(abs(Xft)))
xlabel('Frequência linear f (hertz)')
ylabel('|X_{FT}(f)|')
title('Transformada contínua de Fourier (FT), em módulo, de um sinal de aúdio do oboé')
grid on;
a = axis();
axis([-0.06 * fs 0.06 * fs a(3) a(4)])
xticks = get(gca, 'Xtick')
set(gca, 'Xtick', sort([-440*5 -440*4 -440*3 -440*2 -440*1 0 440 440*2 440*3 440*4 440*5]));








clear all; close all; clc

load('ECG_1.mat', 'x', 'fs');
x = x(:, 1);

N = length(x);
n = 0 : N - 1;
t = n * 1 / fs;
plot(t, x);
xlabel('Tempo (segundos)')
ylabel('x_c(t)')
grid on;

figure;
X = fft(x - mean(x));
f = linspace(-0.5, 0.5, N) * fs;
plot(f, fftshift(abs(X)) / fs)
grid on;
xlabel('Frequência f (hertz)');
ylabel('|Xc(f)|');
a = axis();
axis([0 fs / 2 a(3) a(4)])

clear all; close all; clc

x = 1 : 10;
x = upsample(x, 4);
N = length(x);
n = 0 : N - 1;

stem(n, x)
xlabel('Índices de tempo n')
ylabel('x[n]')
grid on
figure;

% Cálculo da transformada de Fourier em tempo discreto (DTFT)
% x_hat(f): somatório com n de -infinito a infinito de x[n]*exp(-j*2*pi*f*n), para qualquer f real

f = linspace(-3, 3, 100000); % f na teoria é variável contínua; estamos calculando em um número elevado de pontos.
x_hat = zeros(size(f));
for k = 1 : length(f)
    x_hat(k) = sum(x .* exp(-j * 2 * pi * f(k) * n));
end

plot(f, abs(x_hat))
xlabel('Frequências lineares normalizadas f')
ylabel('|X_{DTFT}(f)|')
grid on

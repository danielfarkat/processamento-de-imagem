clear all; close all; clc

[x, fs] = audioread('audio_with_problem.wav');
N = length(x);
n = 0 : N - 1;
t = n * 1.0 / fs;
plot(t, x)
xlabel('Tempo t (s)')
ylabel('x_c(t)')
grid on
close all;

X =  fft(x);
f_normalized = linspace(-0.5, 0.5, N);
f = f_normalized * fs;
X = X / fs;
plot(f, fftshift(abs(X)))
grid on;
xlabel('Frequência f (hertz)')
ylabel('|X_c(f)|')

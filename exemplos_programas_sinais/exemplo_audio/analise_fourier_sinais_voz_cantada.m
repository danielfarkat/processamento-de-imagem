clear all; close all; clc

% Leitura de um sinal de voz
[x, fs]= audioread('voz1.wav');
x = x(:, 1);
N = length(x);
n = 0 : N - 1;
t = n * fs;
plot(t, x);
grid on;
xlabel('Tempo (segundos)');
ylabel('x_c(t)')

% Análise de Fourier
figure;
X = fft(x);
f = linspace(-0.5, 0.5, N) * fs;
plot(f, fftshift(abs(X)) * fs)
xlabel('Frequências (hertz)')
ylabel('|X_c(f)|')
grid on;
a = axis();
axis([0 0.5*10^4 a(3) a(4)])

pause
close all

% Leitura de um sinal de voz
[x, fs]= audioread('voz2.wav');
x = x(:, 1);
N = length(x);
n = 0 : N - 1;
t = n * fs;
plot(t, x);
grid on;
xlabel('Tempo (segundos)');
ylabel('x_c(t)')

% Análise de Fourier
figure;
X = fft(x);
f = linspace(-0.5, 0.5, N) * fs;
plot(f, fftshift(abs(X)) * fs)
xlabel('Frequências (hertz)')
ylabel('|X_c(f)|')
grid on;
a = axis();
axis([0 0.5*10^4 a(3) a(4)])
